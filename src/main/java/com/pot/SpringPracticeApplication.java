package com.pot;

import com.pot.model.Console;
import com.pot.model.PS4;
import com.pot.model.Player;
import com.pot.model.X1;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Configuration
public class SpringPracticeApplication {

    @Bean
    Console x1() {
        return new X1();
    }

    @Bean
    Console ps4() {
        return new PS4();
    }

    @Bean
    Player player() {
        return new Player(ps4());
    }

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(SpringPracticeApplication.class);

        Player player = (Player) context.getBean("player");
        System.out.println(player);

        SpringApplication.run(SpringPracticeApplication.class, args);
    }
}
