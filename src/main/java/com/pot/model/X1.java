package com.pot.model;

import org.springframework.stereotype.Component;

/**
 * Created by priv on 02.02.16.
 */
@Component("x1")
public class X1 implements Console {

    @Override
    public String getName() {
        return "X1";
    }

}
